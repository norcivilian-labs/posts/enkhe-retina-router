### Настройка переключения и маршрутизации роутера в меню.

#### Issues

- https://gitlab.com/norcivilian-labs/retina-tauri/-/issues/1
- https://gitlab.com/norcivilian-labs/retina-tauri/-/issues/2

#### Merge_requests

- https://gitlab.com/norcivilian-labs/retina-tauri/-/merge_requests/1
- https://gitlab.com/norcivilian-labs/retina-tauri/-/merge_requests/2

1.  Создание чернового варианта роутера и его проверка работы.
2.  Изменение названия компонентов меню по макету.
3.  Делаем макет меню резиновым и центрируем, cогласно макету.

### Создание логина, профиля, регистрации.

#### Issues

- https://gitlab.com/norcivilian-labs/retina-tauri/-/issues/3

#### Merge_requests

- https://gitlab.com/norcivilian-labs/retina-tauri/-/merge_requests/4

1. Верстка компонентов по макету.
2. Стилизация компонентов. Делаем его резиновым при помощи флексбокс.

### Делаем структуру проекта

#### Issues

- https://gitlab.com/norcivilian-labs/retina-tauri/-/issues/4

#### Merge_requests

- https://gitlab.com/norcivilian-labs/retina-tauri/-/merge_requests/3

1. Создаем структуру проекта по макету в Penpot.
2. Создаем папки для компонентов проекта.
3. Перемещаем компоненты в отдельные папки.

### Запрос на вход и регистрацию.

#### Issues

- https://gitlab.com/norcivilian-labs/retina-tauri/-/issues/5

#### Merge_requests

- https://gitlab.com/norcivilian-labs/retina-tauri/-/merge_requests/5

В данном таске реализуем функционал формы входа в систему при помощи Preact. При загрузке компонента Login инициализируются состояния для email, пароля и токена с помощью хука useState. При вводе данных пользователем в поля email и пароля значения автоматически обновляются в соответствующих состояниях. При нажатии кнопки "Войти" происходит отправка POST-запроса на сервер для аутентификации пользователя. В случае успешного ответа сервера (статус 200), полученный токен сохраняется в состоянии. В случае ошибки выводится сообщение об ошибке. Компонент предоставляет простой и удобный интерфейс для входа пользователей в систему.

### Исправление названия полей в запросах fetch.

#### Issues

- https://gitlab.com/norcivilian-labs/retina-tauri/-/issues/6

#### Merge_requests

- https://gitlab.com/norcivilian-labs/retina-tauri/-/merge_requests/6
- [ ] 
В данном таске исправляю названия полей в запросе fetch.
Cогласно документации https://retina.docs.norcivilianlabs.org/api.html

### Создание запроса fetch для профиля.

#### Issues

- https://gitlab.com/norcivilian-labs/retina-tauri/-/issues/7

#### Merge_requests

- https://gitlab.com/norcivilian-labs/retina-tauri/-/merge_requests/6

В этом таске мы заменили жестко заданные переменные в запросах fetch на переменные, содержащие значения, введенные или выбранные пользователем. Также добавили запрос fetch, который будет выполнен при открытии экрана профиля. Этот запрос вернет JSON-объект в формате, описанном в документации https://retina.docs.norcivilianlabs.org/api.html#get-user-information, который содержит информацию о пользователе. После получения ответа от сервера мы извлекаем данные из JSON-объекта и сохраняем их в соответствующих переменных.

### Передать токен из экрана логина в меню.

#### Issues

- https://gitlab.com/norcivilian-labs/retina-tauri/-/issues/8

#### Merge_requests

- https://gitlab.com/norcivilian-labs/retina-tauri/-/merge_requests/7

В данном таске мы создаём переменную состояния для токена на уровне корневого компонента приложения (app.jsx) с использованием useState. Затем передаём функцию setToken в компонент экрана Логина (Login screen), где будет устанавливаться токен. После получения токена в экране Логина, его значение передается через состояние другим компонентам (Menu, Profile, Detection, Signup ), которые используют токен.

### Исправить списки стран и профессий.

#### Issues

- https://gitlab.com/norcivilian-labs/retina-tauri/-/issues/9

#### Merge_requests

- https://gitlab.com/norcivilian-labs/retina-tauri/-/merge_requests/8

В этом таске мы изменяем текущий способ задания списка профессий и стран на экране регистрации. Мы воспользуемся JavaScript массивом, содержащий список стран и профессий, согласно документации https://retina.docs.norcivilianlabs.org/api.html#countries-list, чтобы отображать их в виде списка на экране регистрации с помощью метода map(), генерирующего option теги для каждой страны из массива.

